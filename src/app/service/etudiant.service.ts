import { Etudiant } from './../models/Etudiant';
import { url } from './../API_URLs/config';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class EtudiantService {

    etudiant : Etudiant = {}
    constructor(private http : HttpClient){

    }

    //Recuperer la liste des etudiants
    getAllEtudiant() : Observable<any> {
        return this.http.get(url.etudiantsbyPage);
    }

    getEtudiantByPage(page:number) : Observable<any>{
        return this.http.get(url.etudiantsbyPage+page);
    }

    //Recuperer la liste des Licence2
    getEtudiantL2() : Observable<any> {
       return this.http.get(url.etudiantL2) ;
    }
    getEtudiantL2ByPage(page : number) : Observable<any> {
        return this.http.get(url.etudiantL2+ page) ;
     }

      //Recuperer la liste des Licence3
    getEtudiantL3() : Observable<any> {
        return this.http.get(url.etudiantL3) ;
     }
     getEtudiantL3ByPage(page : number) : Observable<any> {
        return this.http.get(url.etudiantL3+ page) ;
     }

     //Recuperer la liste des Master1
     getEtudiantM1() : Observable<any> {
        return this.http.get(url.etudiantM1) ;
     }

     getEtudiantM1ByPage(page : number) : Observable<any> {
        return this.http.get(url.etudiantM1+ page) ;
     }

     //Recuperer la liste des Master2
     getEtudiantM2() : Observable<any> {
        return this.http.get(url.etudiantM2) ;
     }
     getEtudiantM2ByPage(page : number) : Observable<any> {
        return this.http.get(url.etudiantM2+ page) ;
     }
    //Ajouter un etudiant
    addEtudiantL2(etudiant : Etudiant): Observable<any> {
        return this.http.post(url.etudiants, etudiant) ;
    }
    //Supprimer un etudiant
    deleteEtudiantL2(id : number) : Observable<any> {
        return this.http.delete(url.etudiants + `/${id}`) ;
    }
    //Mettre a jour les informations d'un etudiant
    updateEtudiantL2(id:number,etudiant : Etudiant): Observable<any> {
        return this.http.patch(url.etudiants + `/${id}`, etudiant) ; 
    }
    //Recuperer les informations d'un seul étudiant
    getOneEtudiantL2(id:number){
        return this.http.get(url.etudiants + `/${id}` )
    }


    
    savePhoto(file : File) : Observable<any>{
        return this.http.post("http://localhost:8080/savePhoto", file) ;
    }

    

    pagination(etudiant: Array<any>, data:any, page:number, sizeElement : number ) : number{
        etudiant= data ;
        page = data.totalElements / sizeElement ;
        if(data.totalElements % sizeElement !=0) page = (page+(1-((data.totalElements % sizeElement)/10)));
        return page ;
      //  console.log(this.page);
        
        
    }

    searchEtudiant(mc : string) : Observable<any>{
        return this.http.post(url.searchUrl+mc, '') ;
    }

  
    
}