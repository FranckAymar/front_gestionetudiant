
export class Etudiant {
    constructor(public id?: number,
                public nom?: string,
                public prenom?: string,
                public dateNaissance?: string,
                public lieuNaissance? : string,
                public niveau? : string,
                public specialite? : string,
                public sexe? : string,
                public nationnalite? : string,
                public numeroBac? : string,
                public anneeBac? : number,
                public avatar? : string|any
               /* public serie? : string,
                public adresse? : string,
                public telephone?: string,
                public email? : string,
                public status? : string,
                public nomUrgence? : string,
                public contactUrgence? : number*/ ) {}
}