import { EtudiantService } from './service/etudiant.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  etudiantForm : FormGroup;
 

constructor(private formBuilder: FormBuilder, private etudiantService : EtudiantService){};
  title = 'GestionEtudiant';

  showHide : boolean = true ;

  showHideSideBar(){
    this.showHide = !this.showHide ;
    console.log(this.showHide);
  }

  ngOnInit(){
    this.createForm();

  }
 
  createForm(){
    this.etudiantForm = this.formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      dateNaissance: ['', Validators.required],
      lieuNaissance: '',
      //  public niveau? : string,
      specialite: '',
      sexe: '',
      nationnalite: '',
      numeroBac: '',
      anneeBac: '',
      serie: '',
      adresse: '',
      telephone: '',
      email: '',
      status: '',
      nomUrgence: '',
      contactUrgence: '',
      avatar: null ,
      niveau: ''
    });
  }

  
  }


