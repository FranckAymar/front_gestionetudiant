import { HomeComponent } from './home/home.component';
import { ListEtudiantComponent } from './list-etudiant/list-etudiant.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { EtudiantComponent } from './etudiantForm/etudiant.component';
import { EtudiantService} from './service/etudiant.service';
import { RouterModule, Routes} from '@angular/router';
import { ContentComponent } from './content/content.component';
import { HttpClientModule } from '@angular/common/http';
import { L2EtudiantComponent } from './list-etudiant/l2-etudiant/l2-etudiant.component';
import { L3EtudiantComponent } from './list-etudiant/l3-etudiant/l3-etudiant.component';
import { M1EtudiantComponent } from './list-etudiant/m1-etudiant/m1-etudiant.component';
import { M2EtudiantComponent } from './list-etudiant/m2-etudiant/m2-etudiant.component';
import { SearchEtudiantComponent } from './search-etudiant/search-etudiant.component';
const appRouter : Routes = [

  {path: 'home', component: HomeComponent },
  {path: 'addEtu', component: EtudiantComponent},
  {path: 'liste', component:ListEtudiantComponent},
  {path: 'liste/licence2', component: L2EtudiantComponent},
  {path: 'liste/licence3', component: L3EtudiantComponent},
  {path: 'liste/master1', component: M1EtudiantComponent},
  {path: 'liste/master2', component: M2EtudiantComponent},
  {path: 'searchEtudiant', component: SearchEtudiantComponent},
  
]
  


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    EtudiantComponent,
    ContentComponent,
    HomeComponent,
    ListEtudiantComponent,
    L2EtudiantComponent,
    L3EtudiantComponent,
    M1EtudiantComponent,
    M2EtudiantComponent,
    SearchEtudiantComponent,
    
    
  ],
  imports: [
    BrowserModule,FormsModule, RouterModule.forRoot(appRouter), ReactiveFormsModule, HttpClientModule
  ],
  providers: [
    EtudiantService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
