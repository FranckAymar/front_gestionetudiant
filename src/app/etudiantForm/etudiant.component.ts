import { AppComponent } from './../app.component';
import { EtudiantService } from './../service/etudiant.service';
import { Etudiant } from './../models/Etudiant';
import { Component, OnInit} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-etudiant',
  templateUrl: './etudiant.component.html',
  styleUrls: ['./etudiant.component.css']
})


export class EtudiantComponent implements OnInit {

  etudiants: Etudiant[];
  etudiantForm = this.appService.etudiantForm;
  etudiantSelect: Etudiant;
   

  constructor(private formBuilder: FormBuilder, 
              private etudiantService: EtudiantService,
              private router: Router, private appService : AppComponent ) {

    this.appService.createForm();
  }
  
  ngOnInit() {

  }

   
  
  
  saveEtudiant() {
   
    this.etudiantService.addEtudiantL2(this.etudiantForm.value).subscribe(
      (data) => {
        
        console.log(data);
        this.appService.createForm();
        this.router.navigateByUrl('/liste');

      }
    );
  }

  
  
  savePicture(file) {
    
    this.etudiantService.savePhoto(file).subscribe(
      (data) => {
        console.log(data);
        this.appService.createForm();
        this.router.navigateByUrl('/liste');

      }
    );

  
  }
  
  onFileChange(event){
    let reader = new FileReader();
   
    if(event.target.files || event.target.files.lenght > 0){    
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      console.log(file);
      reader.onload = () =>{
        this.etudiantForm.get('avatar').setValue(reader.result

        );
        
      };
      
    }
  
  }


}
