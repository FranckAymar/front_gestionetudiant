import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { L3EtudiantComponent } from './l3-etudiant.component';

describe('L3EtudiantComponent', () => {
  let component: L3EtudiantComponent;
  let fixture: ComponentFixture<L3EtudiantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ L3EtudiantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(L3EtudiantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
