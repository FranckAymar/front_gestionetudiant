import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { M1EtudiantComponent } from './m1-etudiant.component';

describe('M1EtudiantComponent', () => {
  let component: M1EtudiantComponent;
  let fixture: ComponentFixture<M1EtudiantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ M1EtudiantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(M1EtudiantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
