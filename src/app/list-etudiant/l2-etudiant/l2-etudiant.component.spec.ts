import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { L2EtudiantComponent } from './l2-etudiant.component';

describe('L2EtudiantComponent', () => {
  let component: L2EtudiantComponent;
  let fixture: ComponentFixture<L2EtudiantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ L2EtudiantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(L2EtudiantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
