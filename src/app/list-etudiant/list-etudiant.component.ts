import { HttpClient } from '@angular/common/http';
import { AppComponent } from './../app.component';
import { EtudiantService } from './../service/etudiant.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Etudiant } from './../models/Etudiant';
import { Component, OnInit, Injectable } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-list-etudiant',
  templateUrl: './list-etudiant.component.html',
  styleUrls: ['./list-etudiant.component.css']
})

@Injectable()
export class ListEtudiantComponent implements OnInit {


    etudiants   ;
    etudiantSorted = [];
    etudiantSelect : Etudiant ;
    action : string ="add";
    etudiantForm : FormGroup = this.appService.etudiantForm;
    etudiantView ;
    page : number;
    sizeElement : number = 10;
    arrayPage : Array<number> = [];
    currentPage : number  ;
    
    
  constructor(private etudiantService : EtudiantService, 
              private router : Router, 
              private formBuilder: FormBuilder, private appService : AppComponent,
               private http : HttpClient) {
                this.appService.createForm();
                
              }

  ngOnInit() {
    this.loadAllEtudiant();
  
  }

 

 


  loadAllEtudiant(){
    this.etudiantService.getAllEtudiant().subscribe(
      (data)=>{
        this.currentPage = data.page.number ;
        this.etudiants= data ;
        this.arrayPage = new Array(data.page.totalPages);
        
      },
      (error)=>{
        console.log("erreur");
      }
    );
  }

  goToPageEtudiant(index){
    this.etudiantService.getEtudiantByPage(index).subscribe(
      (data)=>{
        this.currentPage = data.page.number ;
       this.etudiants= data;
      },
      (error)=>{
        console.log("erreur");
      }
    );
  }



  deleteEtudiant(id:number){
    if(window.confirm("Etes vous sur ?")){
       this.etudiantService.deleteEtudiantL2(id).subscribe(
      ()=>{

        this.loadAllEtudiant();
       //console.log("Etudiant supprimé");
      }
    );
  }
    
   

  }


  //Recuperer les informations pour les afficher dans les inputs
  selectEtudiantEdit(etudiant : Etudiant){
      //Pour selectionner l'element
      this.etudiantSelect = etudiant;
      //console.log(this.etudiantSelect)
     //this.router.navigateByUrl('/editEtu');
  }

  //Valider l'edition de l'etudiant
  editEtudiant(id:number){
    this.etudiantService.updateEtudiantL2(id, this.etudiantForm.value).subscribe(
      (data)=>{
        this.action = 'add';
        console.log(data);
        this.loadAllEtudiant();
        this.etudiantSelect = new Etudiant();
      }
    )
      
   
    
  }

  //REcuperer un seul etudiant
  getOneEtudiant(id: number){
    this.etudiantService.getOneEtudiantL2(id).subscribe(
      (data)=>{
        this.etudiantSelect = data ;
        console.log(data);
        this.etudiantView = data ;
      }
    );
  }


  //Charger le fichier
  onFileChange(event){
    let reader = new FileReader();
   
    if(event.target.files || event.target.files.lenght > 0){
        console.log('ggggggggg');
        
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      console.log(file);
      reader.onload = () =>{
        this.etudiantForm.get('avatar').setValue(
        
          reader.result
        );
        
      };
      
    }
  
  }


}


