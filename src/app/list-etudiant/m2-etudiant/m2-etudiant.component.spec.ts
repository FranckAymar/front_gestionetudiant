import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { M2EtudiantComponent } from './m2-etudiant.component';

describe('M2EtudiantComponent', () => {
  let component: M2EtudiantComponent;
  let fixture: ComponentFixture<M2EtudiantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ M2EtudiantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(M2EtudiantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
