import { Component, OnInit } from '@angular/core';
import { Etudiant } from 'src/app/models/Etudiant';
import { FormGroup } from '@angular/forms';
import { EtudiantService } from 'src/app/service/etudiant.service';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-m2-etudiant',
  templateUrl: './m2-etudiant.component.html',
  styleUrls: ['./m2-etudiant.component.css']
})
export class M2EtudiantComponent implements OnInit {

  etudiantSorted = [];
  etudiantSelect : Etudiant ;
  action : string ="add";
  etudiantForm : FormGroup = this.appService.etudiantForm;
  etudiantView ;
  page : number;
  sizeElement : number = 10;
  arrayPage : Array<number> = [];
  currentPage : number ;

  constructor(private etudiantService : EtudiantService, 
    private router : Router, private appService: AppComponent) { }

  ngOnInit() {
      this.loadEtudiantM2();
  }
    //Pour avoir une liste lors du premier chargement des etudiants, dans notre cas index = 0
    //pagination() effectue la pagination et retourne le nombre de page
  loadEtudiantM2(){
    this.etudiantService.getEtudiantM2().subscribe(
      (data)=>{
        this.currentPage = data.number ;
        this.etudiantSorted= data;
        this.arrayPage = new Array(this.etudiantService
          .pagination(this.etudiantSorted, data, this.page, this.sizeElement));
        
      }
    )
  }

  goToPageM2Etudiant(index){
   
    if(index==null){index=0};
    this.etudiantService.getEtudiantM2ByPage(index).subscribe(
      (data)=>{
        this.currentPage = data.number ;
        this.etudiantSorted= data;
        
        /*this.etudiantSorted= data ;
        this.page = data.totalElements / this.sizeElement ;
        if(data.totalElements % this.sizeElement !=0) this.page = (this.page+(1-((data.totalElements % this.sizeElement)/10)));
      //  console.log(this.page);
        this.arrayPage = new Array(this.page);
        */
      },
      (error)=>{
        console.log("erreur");
      }
    );
  }

  deleteEtudiant(id:number){
    if(window.confirm("Etes vous sur ?")){
       this.etudiantService.deleteEtudiantL2(id).subscribe(
      ()=>{
        console.log("Etudiant supprimé");
      this.loadEtudiantM2();
       
      }
    );
  }

}

selectEtudiantEdit(etudiant : Etudiant){
  //Pour selectionner l'element
  this.etudiantSelect = etudiant;
  //console.log(this.etudiantSelect)
 //this.router.navigateByUrl('/editEtu');
}

//Valider l'edition de l'etudiant
editEtudiant(id:number){
this.etudiantService.updateEtudiantL2(id, this.etudiantForm.value).subscribe(
  (data)=>{
    this.action = 'add';
    console.log(data);
    this.loadEtudiantM2();
    this.etudiantSelect = new Etudiant();
  }
)
  


}

//REcuperer un seul etudiant
getOneEtudiant(id: number){
this.etudiantService.getOneEtudiantL2(id).subscribe(
  (data)=>{
    this.etudiantSelect = data ;
    console.log(data);
    this.etudiantView = data ;
  }
);
}


//Charger le fichier
onFileChange(event){
let reader = new FileReader();

if(event.target.files || event.target.files.lenght > 0){
    console.log('ggggggggg');
    
  let file = event.target.files[0];
  reader.readAsDataURL(file);
  console.log(file);
  reader.onload = () =>{
    this.etudiantForm.get('avatar').setValue(
    
      reader.result
    );
    
  };
  
}

}

}
