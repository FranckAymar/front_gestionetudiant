import { AppComponent } from './../app.component';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { EtudiantService } from '../service/etudiant.service';
import { Etudiant } from '../models/Etudiant';

@Component({
  selector: 'app-search-etudiant',
  templateUrl: './search-etudiant.component.html',
  styleUrls: ['./search-etudiant.component.css']
})
export class SearchEtudiantComponent implements OnInit {
  searchForm : FormGroup;
  etudiantsSearched : Array<Etudiant> = []
  action : string ='search';
  etudiantView ;
  etudiantSelect;
  etudiantForm : any = this.appService.etudiantForm;
  
  constructor(private etudiantService : EtudiantService,
    private formBuilder: FormBuilder, private appService : AppComponent
    ) { 
      this.createSearchForm();
      this.appService.createForm();
      
       }

  ngOnInit() {

    this.action = 'search' ;
  }

  searchEtudiant(){
    this.etudiantService.searchEtudiant(this.searchForm.get('searchInput').value).subscribe(
      (data)=>{
        
        this.etudiantsSearched = data ;

      }
    )
  }

  createSearchForm(){
    this.searchForm = this.formBuilder.group(
      {
        searchInput : ''
      }
    );
  }


  deleteEtudiant(id:number){
    if(window.confirm("Etes vous sur ?")){
       this.etudiantService.deleteEtudiantL2(id).subscribe(
      ()=>{
        this.searchEtudiant();
        
      
      }
    );
  }
    
   

  }


  //Recuperer les informations pour les afficher dans les inputs
  selectEtudiantEdit(etudiant : Etudiant){
      //Pour selectionner l'element
      this.etudiantSelect = etudiant;
      //console.log(this.etudiantSelect)
     //this.router.navigateByUrl('/editEtu');
  }

  //Valider l'edition de l'etudiant
  editEtudiant(id:number){
    this.etudiantService.updateEtudiantL2(id, this.etudiantForm.value).subscribe(
      (data)=>{
        this.action = 'search';
        this.etudiantSelect = new Etudiant();
      }
    )
      
   
    
  }

  //REcuperer un seul etudiant
  getOneEtudiant(id: number){
    this.etudiantService.getOneEtudiantL2(id).subscribe(
      (data)=>{
        this.etudiantSelect = data ;
        console.log(data);
        this.etudiantView = data ;
      }
    );
  }


  //Charger le fichier
  onFileChange(event){
    let reader = new FileReader();
   
    if(event.target.files || event.target.files.lenght > 0){
        console.log('ggggggggg');
        
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      console.log(file);
      reader.onload = () =>{
        this.etudiantForm.get('avatar').setValue(
        
          reader.result
        );
        
      };
      
    }
  
  }

}
